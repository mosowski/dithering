#include <cstdio>
#include <vector>
#include <iostream>
#include <cmath>
#include "lodepng.h"

#define byte unsigned char

struct Image {
	std::vector<byte> bytes;
	unsigned int width;
	unsigned int height;
	lodepng::State state;
};

Image* load_image(char *filename, Image* img) {
	if (img == NULL) {
		img = new Image();
	}

	std::vector<byte> bytes;
	lodepng::load_file(bytes, filename);
	unsigned int error = lodepng::decode(img->bytes, img->width, img->height, img->state, bytes);
	if (error != 0) {
		std::cout << "error: could not load file " << filename << ": " << lodepng_error_text(error) << std::endl;
		exit(1);
	}

	return img;
}

int round_channel(int value, int bits) {
	int pot = (1 << bits) - 1;
	value >>= (8 - bits);
	return 255 * value / pot;
}

int clamp_byte(int value) {
	return value < 0 ? 0 : (value > 255 ? 255 : value);
}

void dither_image(Image *img, int rBits, int gBits, int bBits, int aBits) {
	byte* bytes = &img->bytes[0];

	int bytesPerPixel = 4;
	if (!lodepng_is_alpha_type(&img->state.info_raw)) {
		bytesPerPixel = 3;
	}
	std::cout << "dithering image with " << bytesPerPixel << " bpp" << " to " << rBits << gBits << bBits << aBits << std::endl;

	int old[] = { 0, 0, 0, 0 };
	int dit[] = { 0, 0, 0, 0 };
	int err[] = { 0, 0, 0, 0 };
	int bits[] = { rBits, gBits, bBits, aBits };

	int kernel[] = { 
		1 * bytesPerPixel,
		(img->width - 1) * bytesPerPixel,
		img->width * bytesPerPixel,
		(img->width + 1) * bytesPerPixel
	};

	int kernelFactor[] = { 7, 3, 5, 1 };

	int length = img->width * img->height * bytesPerPixel;

	for (int j = 0; j < img->height; ++j) {
		for (int i = 0; i < img->width; ++i) {
			int off = (i + j * img->width) * bytesPerPixel;

			for (int c = 0; c < bytesPerPixel; ++c) {
				old[c] = bytes[off + c];
				dit[c] = round_channel(old[c], bits[c]);
				err[c] = old[c] - dit[c];
			}

			for (int k = 0; k < 4; ++k) {
				if (off + kernel[k] < length) {
					for (int c = 0; c < bytesPerPixel; ++c) {
						bytes[off + kernel[k] + c] = clamp_byte(bytes[off + kernel[k] + c] + err[c] * kernelFactor[k] / 16);
					}
				}
			}
		}
	}

	for (int j = 0; j < img->height; ++j) {
		for (int i = 0; i < img->width; ++i) {
			int off = (i + j * img->width) * bytesPerPixel;
			for (int c = 0; c < bytesPerPixel; ++c) {
				bytes[off + c] = round_channel(bytes[off + c], bits[c]);
			}
		}
	}
}

void save_image(Image* img, char* filename) {
	std::vector<byte> bytes;
	lodepng::encode(bytes, img->bytes, img->width, img->height, img->state);
	lodepng::save_file(bytes, filename);
}


int main(int argc, char **argv) {
	if (argc == 7) {
		Image* img = load_image(argv[1], NULL);

		int r = atoi(argv[2]);
		int g = atoi(argv[3]);
		int b = atoi(argv[4]);
		int a = atoi(argv[5]);

		dither_image(img, r, g, b, a);

		save_image(img, argv[6]);
	} else {
		std::cout << "usage:" << std::endl;
		std::cout << "dithering <input_image> <red_bits> <green_bits> <blue_bits> <alpha_bits> <output_image>" << std::endl;
	}
	return 0;
}
